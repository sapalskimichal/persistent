package tree

type nodeInfo struct {
	level, id int
}

// LCA is a wrapper around Tree that allows answering LCA (lowest common ancesstor) queries.
// The algorithm reduces LCA to RMQ (range minimum query), and solves it using a simple min-tree.
// For more details, see for example: http://www.topcoder.com/tc?d1=tutorials&d2=lowestCommonAncestor&module=Static#Reduction%20from%20LCA%20to%20RMQ
type LCA struct {
	Tree
	nodesById       map[int]*Node
	minTree         []nodeInfo
	firstOccurrence map[int]int
}

func minNodeInfo(a, b nodeInfo) nodeInfo {
	if a.level < b.level {
		return a
	}
	return b
}

// BuildLCA wraps the given tree in data structure that enables answering LCA queries.
// The underlying tree will not be modified during the construction of this wrapper.
// Subsequent changes to the tree will not be reflected in the wrapper.
// Note that the nodes must have unique ids.
func BuildLCA(tree Tree) LCA {
	nodesById := make(map[int]*Node)
	n := tree.Size()
	s := 1
	for s < 4*n {
		s *= 2
	}
	minTree := make([]nodeInfo, s/2, s)
	firstOccurrence := make(map[int]int)

	level := 0
	reenter := func(node *Node) {
		minTree = append(minTree, nodeInfo{level, node.Id()})
	}
	enter := func(node *Node) {
		level++
		nodesById[node.Id()] = node
		firstOccurrence[node.Id()] = len(minTree)
		reenter(node)
	}
	exit := func(node *Node) {
		level--
	}
	tree.DFS(enter, reenter, exit)

	for i := len(minTree); i < s; i++ {
		minTree = append(minTree, nodeInfo{n + 1, n + 1})
	}
	for i := s/2 - 1; i > 0; i-- {
		minTree[i] = minNodeInfo(minTree[2*i], minTree[2*i+1])
	}
	return LCA{tree, nodesById, minTree, firstOccurrence}
}

// NodeById returs tree node with given id or nil if it doesn't exist.
func (lca *LCA) NodeById(id int) *Node {
	return lca.nodesById[id]
}

// LCA returns lowest common ancesstor of nodes with given id.
// Running time of this function is O(lg n)
func (lca *LCA) LCA(a, b int) *Node {
	x, y := lca.firstOccurrence[a], lca.firstOccurrence[b]
	if x > y {
		x, y = y, x
	}
	xInfo, yInfo := lca.minTree[x], lca.minTree[y]
	for x/2 != y/2 {
		if x%2 == 0 {
			xInfo = minNodeInfo(xInfo, lca.minTree[x+1])
		}
		if y%2 == 1 {
			yInfo = minNodeInfo(lca.minTree[y-1], yInfo)
		}
		x /= 2
		y /= 2
	}
	return lca.nodesById[minNodeInfo(xInfo, yInfo).id]
}

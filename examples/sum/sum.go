// Solution to a very simple problem (RangeSum): given an array A, build a data structure that would allow answering to queries Sum(a, b) = sum of A[a:b+1] (sum of A[i] where i is in range [a, b]).
// Also, solution to a generalization of this problem (PathSum): given a tree, build a data structure that would allow answering to queries Sum(a, b) = sum of all nodes on a path from a to b (inclusive).
package sum

import (
	"bitbucket.org/sapalskimichal/persistent/examples/framework"
	"bitbucket.org/sapalskimichal/persistent/examples/tree"
)

// RangeSum is a data structure that allows fast range-sum queries on a static array.
type RangeSum interface {
	// Sum returns sum of values[i] where i is in range [a, b] and values represent underlying array.
	// a and b should be from [0, n-1] and a <= b.
	Sum(a, b int) int
}

type rangeSum struct {
	rangeQuery framework.RangeQuery
}

// NewRangeSum creates a new RangeSum structure, the values array is not modified.
func NewRangeSum(values []int) RangeSum {
	elements := make([]framework.Element, 0, len(values))
	for _, v := range values {
		elements = append(elements, v)
	}
	initSweep := func([]framework.Element) framework.Sweep {
		return 0
	}
	updateSweep := func(e framework.Element, s framework.Sweep) framework.Sweep {
		return e.(int) + s.(int)
	}
	answerQuery := func(aSweep, bSweep framework.Sweep, args ...framework.Argument) framework.QueryResult {
		return bSweep.(int) - aSweep.(int)
	}
	return rangeSum{framework.NewRangeQuery(elements, initSweep, updateSweep, answerQuery)}
}

func (ds rangeSum) Sum(a, b int) int {
	return ds.rangeQuery.Query(a, b).(int)
}

// PathSum is a data structure that allows fast path-sum queries on a static tree.
type PathSum interface {
	// Sum returns the sum of all values on path from node a to node b.
	Sum(a, b int) int
}

type pathSum struct {
	pathQuery framework.PathQuery
}

func (p pathSum) Sum(a, b int) int {
	return p.pathQuery.Query(a, b).(int)
}

// NewPathSum creates a new PathSum structure, the tree is not modified.
func NewPathSum(t tree.Tree) PathSum {
	initSweep := func(t tree.Tree) framework.Sweep {
		return 0
	}
	updateSweep := func(node *tree.Node, sweep framework.Sweep) framework.Sweep {
		return sweep.(int) + node.ParentEdge.Data.(int)
	}
	answerQuery := func(a, b, lca framework.Sweep, args ...framework.Argument) framework.QueryResult {
		return a.(int) + b.(int) - 2*lca.(int)
	}

	return pathSum{framework.NewPathQuery(t, initSweep, updateSweep, answerQuery)}
}

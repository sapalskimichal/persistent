package sum

import (
	"bitbucket.org/sapalskimichal/persistent/examples/tree"
	"math"
	"math/rand"
	"testing"
)

func randomRangeSum(n int) (RangeSum, []int) {
	values := make([]int, 0, n)
	for i := 0; i < n; i++ {
		values = append(values, rand.Intn(n))
	}
	return NewRangeSum(values), values
}

func randomRange(n int) (a, b int) {
	a, b = rand.Intn(n), rand.Intn(n)
	if a > b {
		a, b = b, a
	}
	return
}

func TestRangeSum(t *testing.T) {
	n, m := 20, 100
	rand.Seed(17)

	ds, values := randomRangeSum(n)

	for i := 0; i < m; i++ {
		ans := 0
		a, b := randomRange(n)
		for j := a; j <= b; j++ {
			ans += values[j]
		}
		if res := ds.Sum(a, b); res != ans {
			t.Errorf("Random test no %v: Init(%v).Sum(%v, %v) = %v, want %v", i, values, a, b, res, ans)
		}
	}
	panicked := false
	defer func() {
		if r := recover(); r != nil {
			panicked = true
		}
	}()
	ds.Sum(-7, -9)
	if !panicked {
		t.Error("Not panicked with invalid range: [-7, -9]")
	}
}

func BenchmarkRangeSum(b *testing.B) {
	n := 1000000
	ds, _ := randomRangeSum(n)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		a, b := randomRange(n)
		ds.Sum(a, b)
	}
}

func randomPathSum(n int) (PathSum, tree.Tree) {
	depth := int(math.Sqrt(float64(n)) + 2)
	t := tree.RandomTree(n, depth)
	visit := func(node *tree.Node) {
		p := node.ParentEdge
		if p != nil {
			p.Data = rand.Intn(n)
		}
	}
	t.DFS(visit, tree.EmptyVisitor, tree.EmptyVisitor)
	return NewPathSum(t), t
}

func TestPathSum(t *testing.T) {
	n := tree.NewNode
	c := tree.NewChild
	tree := tree.Tree{n(1, nil, c(7, 2, nil, c(10, 3, nil), c(20, 4, nil)), c(3, 5, nil), c(7, 6, nil))}
	ds := NewPathSum(tree)
	tests := []struct{ a, b, result int }{
		{1, 1, 0},
		{4, 3, 30},
		{5, 2, 10},
		{5, 6, 10},
	}

	for i, test := range tests {
		if result := ds.Sum(test.a, test.b); result != test.result {
			t.Errorf("Test %v: PathSum(%v, %v) = %v, want %v", i, test.a, test.b, result, test.result)
		}
	}
}

func BenchmarkPathSum(b *testing.B) {
	n := 1000000
	ds, _ := randomPathSum(n)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		a, b := rand.Intn(n), rand.Intn(n)
		ds.Sum(a, b)
	}
}

func bruteForcePathSum(t tree.Tree, a, b int) int {
	result := 0
	visitor := func(v, p *tree.Node) {
		if v.Id() == a || v.Id() == b {
			if v.Id() == a && v.Id() == b {
				a = -1
				b = -1
			} else {
				result += v.ParentEdge.Data.(int)
			}
		}
		if v.Id() == a {
			a = p.Id()
		}
		if v.Id() == b {
			b = p.Id()
		}
	}
	t.ParentDFS(tree.EmptyParentVisitor, tree.EmptyParentVisitor, visitor)
	return result
}

func TestPathSumBruteForce(t *testing.T) {
	n, m := 20, 30
	for i := 1; i <= n; i++ {
		ds, tr := randomPathSum(i)
		for j := 0; j < m; j++ {
			a, b := rand.Intn(i), rand.Intn(i)
			ans := bruteForcePathSum(tr, a, b)
			if res := ds.Sum(a, b); res != ans {
				t.Errorf("Test %v.%v: PathSum(%v, %v) = %v, want %v", i, j, a, b, res, ans)
			}
		}
	}
}

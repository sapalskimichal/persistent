// tstring package implements strings stored in a balanced binary tree. While not the most efficient for standard use, this may be helpful for large sets of similar, very long strings.
package tstring

import (
	"bitbucket.org/sapalskimichal/persistent/tree"
	"bitbucket.org/sapalskimichal/persistent/tree/balancing"
	"fmt"
	"math/big"
	"math/rand"
	"strconv"
	"strings"
)

type node struct {
	character rune
	size      int64
	p         *big.Int
	x         *big.Int
	hash      *big.Int
}

// HashingConfiguration configures hashing for the strings. To ensure small chance of error, choose appropriate hashing configuration.
type HashingConfiguration struct {
	// P is a prime number - the bigger P the lower chance of collision, but longer running time.
	P *big.Int
	// X should be a random value from [0,P)
	X *big.Int
}

func (a node) CombineWith(bd tree.Data) tree.Data {
	b := bd.(node)
	nx := big.NewInt(0)
	nx.Mul(a.x, b.x)
	nx.Mod(nx, a.p)
	nhash := big.NewInt(0)
	nhash.Mul(a.hash, b.x)
	nhash.Add(nhash, b.hash)
	return node{'#', a.size + b.size, a.p, nx, nhash}
}

// TString type represents string stored in a balanced binary tree.
type TString struct {
	tree balancing.Tree
}

// Empty is an empty TString.
var Empty = New("", P32)

func config(p string) HashingConfiguration {
	dig := make([]string, len(p))
	for i := 0; i < len(p); i++ {
		dig[i] = strconv.Itoa(rand.Intn(10))
	}
	pi, xi := new(big.Int), new(big.Int)
	pi.SetString(p, 10)
	xi.SetString(strings.Join(dig, ""), 10)
	xi.Mod(xi, pi)
	return HashingConfiguration{pi, xi}
}

// Sample hashing configurations. The numbers represent approximate number of bits of the prime (Eg. P32 is a 32-bit prime).
var (
	P32  = config("2147483647")
	P64  = config("18446744073709551557")
	P128 = config("340282366920938463463374607431768211297")
	P256 = config("115792089237316195423570985008687907853269984665640564039457584007913129639747")
	P512 = config("10719998727161395580983799878632319305966866366260995840179875490024700411474887463050800375572447507696100299802037505266219548183418442558607880368747877")
)

// New returns a new TString containing the given string. Running time is O(n).
func New(s string, c HashingConfiguration) TString {
	nodes := make([]tree.Data, 0, len(s))
	for _, r := range s {
		h := big.NewInt(int64(r))
		h.Mod(h, c.P)
		nodes = append(nodes, node{r, 1, c.P, c.X, h})
	}
	return TString{balancing.FromSlice(nodes)}
}

func first(f tree.Finger) (tree.Finger, error) {
	for !f.IsLeaf() {
		f, _ = f.Left()
	}
	return f, nil
}

func next(f tree.Finger) (tree.Finger, error) {
	for !f.IsLeft() {
		var err error
		f, err = f.Up()
		if err != nil {
			return nil, err
		}
	}
	f, _ = f.Up()
	f, _ = f.Right()
	return first(f)
}

// String returns the text stored in this TString. Running time is O(n).
func (t TString) String() string {
	l := t.Len()
	if l == 0 {
		return ""
	}
	letters := make([]string, 0, l)
	for f, err := first(t.tree.Root()); err == nil; f, err = next(f) {
		d, _ := f.Data()
		letters = append(letters, string(d.(node).character))
	}
	return strings.Join(letters, "")
}

// Len returns the length of this string. Running time is O(1).
func (t TString) Len() int64 {
	rootData, err := t.tree.Root().Data()
	if err != nil {
		return 0
	}
	return rootData.(node).size
}

func (t TString) runeAtFinger(k int64) (tree.Finger, error) {
	if k < 0 || k >= t.Len() {
		return nil, fmt.Errorf("Rune index not in range: %v, [0, %v)", k, t.Len())
	}
	f := t.tree.Root()
	for !f.IsLeaf() {
		l, _ := f.Left()
		d, _ := l.Data()
		s := d.(node).size
		if s <= k {
			k -= s
			f, _ = f.Right()
		} else {
			f, _ = f.Left()
		}
	}
	return f, nil
}

// RuneAt retruns k-th (0-based) rune in the string. Specifying an index outside [0,Len()) results in an error. Running time is O(lg(n)).
func (t TString) RuneAt(k int64) (rune, error) {
	f, err := t.runeAtFinger(k)
	if err != nil {
		return ' ', err
	}
	d, _ := f.Data()
	return d.(node).character, nil
}

// SetRuneAt returns a new string, whth k-th rune changed to newRune. The old string is not modified. Specifying an index outside [0,Len()) results in an error. Running time and memory complexity is O(lg(n)).
func (t TString) SetRuneAt(k int64, newRune rune) (TString, error) {
	f, err := t.runeAtFinger(k)
	if err != nil {
		return Empty, err
	}
	d, _ := f.Data()
	dn := d.(node)
	h := big.NewInt(int64(newRune))
	h.Mod(h, dn.p)
	nt, _ := t.tree.Update(f, node{newRune, 1, dn.p, dn.x, h})
	return TString{nt}, nil
}

// Split splits the string into two parts: first k runes and the other runes. The old string is not modified. Specifying an index outside [0,Len()] results in an error. Running time and memory complexity is O(lg(n)).
func (t TString) Split(k int64) (TString, TString, error) {
	if k == t.Len() {
		return t, Empty, nil
	}
	f, err := t.runeAtFinger(k)
	if err != nil {
		return Empty, Empty, err
	}
	ta, tb, _ := t.tree.SplitRight(f)
	return TString{ta}, TString{tb}, nil
}

// Concat concatenates this string with the given string. Both strings have to have the same configuration. The strings are not modified. Running time and memory complexity is O(lg(n)), or to be precise: O(lg(max(n,m)/(1 + min(n,m))) where n and m are the lenghts of the strings.
func (t TString) Concat(r TString) TString {
	return TString{t.tree.Merge(r.tree)}
}

// Equals checks if the two strings are equal. Note that this is a probabilistic function - there is at most max(n, m) / P chance that it will return "true" even though the strings are different. If the strings are equal, it will always return "true". Both strings should have the same configuration. Running time is O(1).
func (t TString) Equal(r TString) bool {
	if t.Len() != r.Len() {
		return false
	}
	if t.Len() == 0 {
		return true
	}
	d1, _ := t.tree.Root().Data()
	d2, _ := r.tree.Root().Data()
	return d1.(node).hash.Cmp(d2.(node).hash) == 0
}

// Less checks if this string is less than the given string. Note that this is a probabilistic function - there is at most max(n, m) * log(min(n, m)) / P chance that it will return a wrong answer. Both strings should have the same configuration. Running time is O(log(m) * log(n)).
func (t TString) Less(r TString) bool {
	equalPrefix := func(l int64) bool {
		ts, _, _ := t.Split(l)
		rs, _, _ := r.Split(l)
		return ts.Equal(rs)
	}
	n := t.Len()
	if r.Len() < n {
		n = r.Len()
	}
	if equalPrefix(n) {
		return t.Len() < r.Len()
	}
	beg, end := int64(0), n
	for end-beg > 1 {
		mid := (beg + end) / 2
		if equalPrefix(mid) {
			beg = mid
		} else {
			end = mid
		}
	}
	tr, _ := t.RuneAt(beg)
	rr, _ := r.RuneAt(beg)
	return tr < rr
}

// Package count implements solution for range element count query problem and path element count query problem.
package count

import (
	"bitbucket.org/sapalskimichal/persistent/examples/framework"
	"bitbucket.org/sapalskimichal/persistent/examples/tree"
	"bitbucket.org/sapalskimichal/persistent/treemap"
)

// RangeElementCount represents solution to range element count problem: given an array, preprocess it to quickly answer ElementCount and IntervalCount queries.
type RangeElementCount interface {
	// ElementCount returns how many times an element occurs in the range [a,b] of the underlying array.
	ElementCount(element string, a, b int) int
	// IntervalCount returns how many times an element from interval [x,y] occurs in the range [a,b] of the underlying array.
	IntervalCount(x, y string, a, b int) int
}

type rangeElementCount struct {
	rangeQuery framework.RangeQuery
}

func (r *rangeElementCount) ElementCount(element string, a, b int) int {
	return r.rangeQuery.Query(a, b, element).(int)
}

func (r *rangeElementCount) IntervalCount(x, y string, a, b int) int {
	return r.rangeQuery.Query(a, b, x, y).(int)
}

// NewRangeElementCount constructs a solution to range element count problem for a given array.
func NewRangeElementCount(items []string) RangeElementCount {
	elements := make([]framework.Element, 0, len(items))
	itemsMap := make(map[string]int)
	for _, v := range items {
		elements = append(elements, v)
		itemsMap[v] = 0
	}

	initSweep := func([]framework.Element) framework.Sweep {
		return treemap.NewStaticStringSummingMap(itemsMap)
	}
	updateSweep := func(e framework.Element, s framework.Sweep) framework.Sweep {
		m := s.(treemap.Map)
		count, _ := m.Get(e)
		sweep, _ := s.(treemap.SummingMap).Update(e, count.(int)+1)
		return sweep
	}
	answerQuery := func(aSweep, bSweep framework.Sweep, args ...framework.Argument) framework.QueryResult {
		if len(args) == 1 {
			element := args[0].(string)
			aCount, aErr := aSweep.(treemap.Map).Get(element)
			bCount, bErr := bSweep.(treemap.Map).Get(element)
			if aErr != nil || bErr != nil {
				return 0
			}
			return bCount.(int) - aCount.(int)
		} else {
			x := args[0].(string)
			y := args[1].(string)
			if x > y {
				return 0
			}
			aCount := aSweep.(treemap.SummingMap).Sum(x, y)
			bCount := bSweep.(treemap.SummingMap).Sum(x, y)
			return bCount - aCount
		}
	}
	return &rangeElementCount{framework.NewRangeQuery(elements, initSweep, updateSweep, answerQuery)}
}

// PathElementCount implements solution to path element count problem: preprocess a tree to quickly answer ElementCount queries (how many times an element occurs on the path between two nodes) and IntervalCount queries (how many times an element from interval [x,y] occurs on the path between two nodes).
type PathElementCount interface {
	// ElementCount counts how many times the element occurs on the edges of the path between nodes a and b in the underlying tree.
	ElementCount(element string, a, b int) int
	// IntervalCount counts how many times elements from interval [x,y] occur on the edges of the path between nodes a and b in the underlying tree.
	IntervalCount(x, y string, a, b int) int
}

type pathElementCount struct {
	pathQuery framework.PathQuery
}

func (p *pathElementCount) ElementCount(element string, a, b int) int {
	return p.pathQuery.Query(a, b, element).(int)
}

func (p *pathElementCount) IntervalCount(x, y string, a, b int) int {
	return p.pathQuery.Query(a, b, x, y).(int)
}

// NewPathElementCount returns a solution for path element count problem for a given tree.
func NewPathElementCount(t tree.Tree) PathElementCount {
	elementMap := make(map[string]int)
	visit := func(n *tree.Node) {
		e := n.ParentEdge
		if e != nil {
			elementMap[e.Data.(string)] = 0
		}
	}
	t.DFS(visit, tree.EmptyVisitor, tree.EmptyVisitor)
	initSweep := func(_ tree.Tree) framework.Sweep {
		return treemap.NewStaticStringSummingMap(elementMap)
	}
	updateSweep := func(n *tree.Node, sweep framework.Sweep) framework.Sweep {
		s := n.ParentEdge.Data.(string)
		count, _ := sweep.(treemap.Map).Get(s)
		newSweep, _ := sweep.(treemap.Map).Update(s, count.(int)+1)
		return newSweep
	}
	answerQuery := func(a, b, lca framework.Sweep, args ...framework.Argument) framework.QueryResult {
		if len(args) == 1 {
			element := args[0].(string)
			aCount, aErr := a.(treemap.Map).Get(element)
			bCount, bErr := b.(treemap.Map).Get(element)
			lcaCount, lcaErr := lca.(treemap.Map).Get(element)
			if aErr != nil || bErr != nil || lcaErr != nil {
				return 0
			}
			return aCount.(int) + bCount.(int) - 2*lcaCount.(int)
		} else {
			x := args[0].(string)
			y := args[1].(string)
			if x > y {
				return 0
			}
			aSum := a.(treemap.SummingMap).Sum(x, y)
			bSum := b.(treemap.SummingMap).Sum(x, y)
			lcaSum := lca.(treemap.SummingMap).Sum(x, y)
			return aSum + bSum - 2*lcaSum
		}
	}

	return &pathElementCount{framework.NewPathQuery(t, initSweep, updateSweep, answerQuery)}
}

// lctree package implements link-cut tree structure for the Minimum Spanning Forest Problem.
package lctree

// NodeId type represents a node in the link-cut tree structure.
type NodeId int32

// Weight type represents edge weight in the link-cut tree structure.
type Weight interface{}

// WeightComparator type allows to compare weights.
type WeightComparator interface {
	// Less checks if w1 < w2.
	Less(w1, w2 Weight) bool
}

// LinkCut interface represents the link-cut tree structure.
type LinkCut interface {
	// NewNode creates a new node and returns its NodeId.
	NewNode() NodeId
	// Root returns the root of the tree in which given node currently resides. If v does not exist it will be created.
	Root(v NodeId) NodeId
	// Evert makes the given node the root of its tree by reversing the path to the current root. If v does not exist it will be created.
	Evert(v NodeId)
	// Link adds an edge with given weight between v and u. This will make v a child of u. If v is not the root of its tree, it will be made the root before linking. If v and u are in the same tree this operation will do nothing. If v or u do not exist they will be created.
	Link(v, u NodeId, weight Weight)
	// Cut removes the edge from v to its parent. If v is the root of its tree, this operation does nothing. If v does not exist it will be created.
	Cut(v NodeId)
	// MaxParentLink returns the node u on the path between v and the root of its tree such that the weight of u's parent link is the greatest possible. If v is the root, v is returned along with nil weight. If v does not exist it will be created.
	MaxParentLink(v NodeId) (u NodeId, weight Weight)
}

type weightInfo struct {
	id     NodeId
	weight Weight
	min    bool
}

func max(wc WeightComparator, w ...weightInfo) weightInfo {
	res := w[0]
	for i := 1; i < len(w); i++ {
		if w[i].min {
			continue
		}
		if res.min || wc.Less(res.weight, w[i].weight) {
			res = w[i]
		}
	}
	return res
}

// node represents a single node in the link-cut tree.
type node struct {
	// Node id.
	id NodeId
	// Weight of the preferred edge down from this node.
	downWeight weightInfo
	// Weight of the preferred edge up from this node or the weight of the up link if this is the last node on the preferred path.
	upWeight weightInfo
	// Link up the representation tree. This is meaningful only for the last node of splay tree.
	up *node

	// If the node should be reversed (swap up and down, left and right).
	toReverse bool
	// Left, right and parent nodes in the splay tree which stores the preferred path.
	left, right, parent *node
	// Maximum weight in the splay subtree of this node.
	subtreeWeight weightInfo
	// WeightComparator for comparing weights.
	comparator WeightComparator
}

type linkCut struct {
	nodes      map[NodeId]*node
	lastId     NodeId
	comparator WeightComparator
}

func (lc *linkCut) getNode(v NodeId) *node {
	if lc.nodes[v] == nil {
		weight := weightInfo{v, nil, true}
		lc.nodes[v] = &node{v, weight, weight, nil, false, nil, nil, nil, weight, lc.comparator}
	}
	return lc.nodes[v]
}

func (v *node) getSubtreeWieght(wc WeightComparator) weightInfo {
	if v == nil {
		return weightInfo{-1, nil, true}
	}
	return v.subtreeWeight
}

func (v *node) reverse() {
	if v != nil {
		v.toReverse = !v.toReverse
	}
}

// fix performs delayed reverse and updates subtreeWeight.
func (v *node) fix() {
	if v == nil {
		return
	}
	if v.toReverse {
		v.left, v.right = v.right, v.left
		v.upWeight, v.downWeight = v.downWeight, v.upWeight
		v.left.reverse()
		v.right.reverse()
		v.toReverse = false
	}
	v.subtreeWeight = max(v.comparator, v.upWeight, v.downWeight, v.right.getSubtreeWieght(v.comparator), v.left.getSubtreeWieght(v.comparator))
}

// fixUp performs fix on the path from the root of the splay tree to v.
func (v *node) fixUp() {
	if v.parent != nil {
		v.parent.fixUp()
	}
	v.fix()
}

// cutDown splits preferred path below this node.
func (v *node) cutDown() {
	v.splay()
	v.left.fix()
	if v.left != nil {
		v.left.parent = nil
		v.left.highestPreferred().up = v
	}
	v.downWeight = weightInfo{v.id, nil, true}
	v.left = nil
	v.fix()
}

// access puts this node on the preferred path from the root.
func (v *node) access() {
	v.cutDown()
	for v.highestPreferred().up != nil {
		r := v.highestPreferred()
		p := r.up
		p.cutDown()
		p.downWeight = r.upWeight
		r.up = nil

		v.splay()
		p.left = v
		v.parent = p
		p.fix()
		v = p
	}
}

// isLeft checks if this node is a left child in its splay tree.
func (v *node) isLeft() bool {
	v.parent.fix()
	v.fix()
	return v.parent.left == v
}

// zig rotates v up the splay tree.
func (v *node) zig() {
	p := v.parent
	if v.isLeft() {
		p.left = v.right
		if v.right != nil {
			v.right.parent = p
		}
		v.right = p
	} else {
		p.right = v.left
		if v.left != nil {
			v.left.parent = p
		}
		v.left = p
	}
	if p.parent != nil {
		if p.isLeft() {
			p.parent.left = v
		} else {
			p.parent.right = v
		}
	}
	v.parent = p.parent
	p.parent = v
	p.fix()
	v.fix()
}

// splay rearranges the splay tree so that v becomes its root.
func (v *node) splay() *node {
	v.fixUp()
	for v.parent != nil && v.parent.parent != nil {
		if v.isLeft() == v.parent.isLeft() {
			v.parent.zig()
			v.zig()
		} else {
			v.zig()
			v.zig()
		}
	}
	if v.parent != nil {
		v.zig()
	}
	v.fix()
	return v
}

func rightmost(u *node) *node {
	u.fix()
	if u.right != nil {
		return rightmost(u.right)
	}
	return u
}

// highestPreferred returns the highest node on v's preferred path.
func (v *node) highestPreferred() *node {
	v.splay()
	return rightmost(v).splay()
}

func (lc *linkCut) NewNode() NodeId {
	for lc.nodes[lc.lastId] != nil {
		lc.lastId++
	}
	return lc.getNode(lc.lastId).id
}

func (lc *linkCut) Root(v NodeId) NodeId {
	vn := lc.getNode(v)
	vn.access()
	vn.splay()
	return vn.highestPreferred().id
}

func (lc *linkCut) Evert(v NodeId) {
	vn := lc.getNode(v)
	vn.access()
	vn.splay().reverse()
}

func (lc *linkCut) Link(v, u NodeId, weight Weight) {
	if lc.Root(v) == lc.Root(u) {
		return
	}
	lc.Evert(v)
	vn, un := lc.getNode(v), lc.getNode(u)
	vn.access()
	vn.up = un
	vn.upWeight = weightInfo{vn.id, weight, false}
	vn.fix()
}

func leftmost(u *node) *node {
	u.fix()
	if u == nil || u.left == nil {
		return u
	}
	return leftmost(u.left)
}

func (lc *linkCut) Cut(v NodeId) {
	vn := lc.getNode(v)
	vn.access()
	vn.splay()
	p := leftmost(vn.right)
	if p == nil {
		return
	}
	p.access()
	vn.up = nil
	vn.upWeight = weightInfo{vn.id, nil, true}
	vn.fix()
}

func (lc *linkCut) MaxParentLink(v NodeId) (u NodeId, weight Weight) {
	vn := lc.getNode(v)
	vn.access()
	vn.splay()
	w := vn.subtreeWeight
	if w.min {
		return v, nil
	}
	r := lc.getNode(w.id)
	r.fixUp()
	if r.upWeight.min || lc.comparator.Less(r.upWeight.weight, w.weight) {
		r.splay()
		rr := rightmost(r.left).splay()
		r = rr
	}
	return r.id, r.upWeight.weight
}

// NewLinkCut returns a new link-cut structure.
func NewLinkCut(comparator WeightComparator) LinkCut {
	return &linkCut{make(map[NodeId]*node), 0, comparator}
}

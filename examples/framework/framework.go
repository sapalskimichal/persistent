// A general framework for implementing Range and Path queries.
// Two main functions in this module are NewRangeQuery and NewPathQuery.
package framework

import (
	"bitbucket.org/sapalskimichal/persistent/examples/tree"
	"fmt"
)

// Element is a data type for elements of the input array.
type Element interface{}

// Sweep is a data type for the sweep.
type Sweep interface{}

// Argument is a data type for additional arguments to query.
type Argument interface{}

// QueryResult is a data type for the result of the query.
type QueryResult interface{}

// RangeQuery is type of representing a solution to a query problem.
// It can be constructed using NewRangeQuery function.
type RangeQuery interface {
	// Answer a query given a range [a,b] of indices of the underlying array.
	Query(a, b int, args ...Argument) QueryResult
}

// Types for arguments to NewRangeQuery function. See it's documentation for details.
type InitRangeQuerySweep func(elements []Element) Sweep
type UpdateRangeQuerySweep func(element Element, currentSweep Sweep) Sweep
type AnswerRangeQuery func(aSweep, bSweep Sweep, arguments ...Argument) QueryResult

type rangeQuery struct {
	sweepStates []Sweep
	answerQuery AnswerRangeQuery
}

func (r *rangeQuery) Query(a, b int, arguments ...Argument) QueryResult {
	if a > b || a < 0 || b >= len(r.sweepStates) {
		panic(fmt.Sprintf("Invalid range: [%v, %v]", a, b))
	}
	return r.answerQuery(r.sweepStates[a], r.sweepStates[b+1], arguments...)
}

// NewRangeQuery takes an array and specification of the range query problem and constructs a RangeQuery object.
// The construction is the following: initSweep function is called with the given array to construct initial state of the sweep.
// Then, for each element of the underlying array, updateSweep function is called with the current sweep state and should return updated sweep (after processing given element). Note that current sweep state should not be modified.
// To answer a query for a range [a,b], answerQuery function is given the sweep states as it were just before processing a-th element and just after processing b-th element. Additional arguments are passed without modification.
func NewRangeQuery(elements []Element, initSweep InitRangeQuerySweep, updateSweep UpdateRangeQuerySweep, answerQuery AnswerRangeQuery) RangeQuery {
	sweepStates := make([]Sweep, 0, len(elements)+1)
	sweepStates = append(sweepStates, initSweep(elements))
	for i, element := range elements {
		sweepStates = append(sweepStates, updateSweep(element, sweepStates[i]))
	}
	return &rangeQuery{sweepStates, answerQuery}
}

// PathQuery is type of representing a solution to a query problem.
// It can be constructed using NewPathQuery function.
type PathQuery interface {
	// Answer a query given two node ids, which are the ends of queried path in the underlying tree.
	Query(a, b int, args ...Argument) QueryResult
}

// Types for arguments to NewPathQuery function. See it's documentation for details.
type InitPathQuerySweep func(t tree.Tree) Sweep
type UpdatePathQuerySweep func(node *tree.Node, currentSweep Sweep) Sweep
type AnswerPathQuery func(a, b, lca Sweep, arguments ...Argument) QueryResult

type pathQuery struct {
	sweepInfo   map[int]Sweep
	lca         tree.LCA
	answerQuery AnswerPathQuery
}

func (p *pathQuery) Query(a, b int, args ...Argument) QueryResult {
	return p.answerQuery(p.sweepInfo[a], p.sweepInfo[b], p.sweepInfo[p.lca.LCA(a, b).Id()], args...)
}

// NewPathQuery function takes a tree and specification of path query problem and returns a PathQuery object.
// The construction is the following: initSweep is called on the tree to get initial sweep state. The sweep is saved with the root node.
// Then, for each non-root node, the updateSweep function is called. The node's parent has already been processed and sweep state after processing the parent is passed to updateSweep function. The function shouldn't modify this sweep state, but should process the node and return a new sweep state.
// To answer a query, answerQuery function will be called with Sweep objects for: beginnig of the path, end of the path and their lowest common ancesstor (LCA). Additional arguments are passed without modification.
func NewPathQuery(t tree.Tree, initSweep InitPathQuerySweep, updateSweep UpdatePathQuerySweep, answerQuery AnswerPathQuery) PathQuery {
	lca := tree.BuildLCA(t)
	sweepInfo := make(map[int]Sweep)

	visit := func(node, parent *tree.Node) {
		var sweep Sweep
		if parent != nil {
			sweep = sweepInfo[parent.Id()]
			sweepInfo[node.Id()] = updateSweep(node, sweep)
		} else {
			sweepInfo[node.Id()] = initSweep(t)
		}
	}
	t.ParentDFS(visit, tree.EmptyParentVisitor, tree.EmptyParentVisitor)
	return &pathQuery{sweepInfo, lca, answerQuery}
}
